# Rootless docker

Starting v20.10, docker has a "rootless mode" considered stable.


## Installation

Follow the official instructions: https://docs.docker.com/engine/security/rootless/

## Notes

It seems the default rootless docker behavior in term of uid translation is to have the root (uid 0) container translated to the host user (the one you are using to launch the container).

#### Example: Subuid/subgid setup

```
host# cat /etc/subuid
<user>:100000:65536

host# cat /etc/subgid
<user>:100000:65536
```

#### Getting the actual uid\_map of a docker container

```
host# id
  uid=1000(<user>) ... # host user id is 1000 here

host# docker run ... # launching a container

host# docker ps # getting container ID

  CONTAINER ID   IMAGE           COMMAND                  CREATED             STATUS             PORTS                                       NAMES
  f629c4afd3e2   debian:latest   "/bin/bash"              16 minutes ago      Up 16 minutes                                                  keen_banzai

host# docker inspect -f '{{.State.Pid}}' f629c4afd3e2
  10259

host# cat /proc/10259/uid_map

         0       1000          1
         1     100000      65536

```

This is probably related to docker using `rootlesskit` that has this behavior by default:

https://github.com/rootless-containers/rootlesskit#usage


