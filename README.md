# Docker as a local environment

A "so-tiny-it-is-not" project that provides some hints when trying to use docker images as local building/compiling environments.

The goal is to:
- Have a reproductible way to build,
- Use source code from a local machine directory, to minimize the storage footprint and be able to use some beloved IDE,
- Do not dirty your workspace with files with undesired owner/group.

## Requirements

You need to install:
- a "recent" Docker Engine version (1.10+). You can follow [these instructions](https://docs.docker.com/engine/installation/).
- [docker-compose](https://docs.docker.com/compose/install/).

**However**, it is strongly suggested to configure your local Docker service [to use a user namespace](./doc/docker_namespace.md), or use [Docker in rootless mode](./doc/docker_rootless.md).

### Principle

- use the `bin/denv` script. You can add it to your `PATH` environment variable (update `PATH`, use a symbolic link, etc.)
- launch `denv list` to list environments
- launch `denv <environment>` to start an environment
- this will launch a docker container with:
  - the current directory mounted on the docker container,
  - the `WORKSPACE` directory mounted if the `WORKSPACE` environment variable is defined,
  - alternatively, you can specify another exported folder with `denv <environment> <exported_folder> ...`.

**Note**: the script should not be used with the HOME directly mounted, as the docker images can pollute your dotfiles/caches a lot. You do not want to mess your favorite user environment, do you?

### Usage

Examples:

```bash
$ denv list           # list environments
$ denv ubuntu         # launch a ubuntu container with the WORKSPACE directory mounted
$ denv ubuntu cmd ... # directly launch cmd inside a ubuntu container
```

### VLC Building usage

See [these instructions](./doc/vlc.md)

### Customize the environment

- you can define a `DENV_DIR` to your environment to choose the path to the `denv` configuration
- inside `DENV_DIR`:
  - `local_env.yml` defines the global configuration of your environments. you can add some additional settings (`environment`, for example) if you want
  - all environment profiles are standard `docker-compose` configuration files inside `$DENV_DIR/profiles`.
  - to create a new environment, just add a standard `docker-compose` configuration file inside `$DENV_DIR/profiles`. `denv` assumes the service to be run is call `app`.
  - VLC compile environments uses a weird `VLC_LATEST` tag. Check the `denv` source code to find out why.
- if present, `DENV_COMPOSE_COMMAND` will replace the default `compose` command
- if present, `DENV_COMPOSE_RUN_OPTIONS` will add options the `compose run` command
